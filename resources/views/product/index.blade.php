<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <title>Tes Developer Vending Machine</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
    <style type="text/css">
        .table1 {
            font-family: sans-serif;
            color: #444;
            border-collapse: collapse;
            width: 50%;
            border: 1px solid #f2f5f7;
        }
     
        .table1 tr th{
            background: #35A9DB;
            color: #fff;
            font-weight: normal;
        }
     
        .table1, th, td {
            padding: 8px 20px;
            text-align: center;
        }
     
        .table1 tr:hover {
            background-color: #f5f5f5;
        }
         
        .table1 tr:nth-child(even) {
            background-color: #f2f2f2;
        }
    </style>
    <script LANGUAGE="JavaScript">
        function validasi(){
            if(form.item_name.value == "" ){
                alert("Anda Belum Memilih Item"); //jika angka kosong maka pesan akan tampil
                exit;
            }
            if(form.stock.value==0 ){
                form.total.value = ""; 
                alert("Item Habis"); //jika angka kosong maka pesan akan tampil
                exit;
            }
        }
       
        function buy(){
            if(form.total.value == "" ){
                alert("Anda Belum Masukan Uang"); //jika angka kosong maka pesan akan tampil
                exit;
            }
        }
        function validasi_bayar(){
            buy();
        }
        function pecahan1() {
            validasi();
            if (form.total.value=="") {
                a=eval(form.submit1.value); 
                b=0; 
                c=a+b 
                form.total.value = c; 
      
            }else{
                a=eval(form.total.value); 
                b=eval(form.submit1.value); 
                c=a+b 
                form.total.value = c; 
                if(c>form.price.value){
                    form.change_money.value = form.total.value - form.price.value; 
                    exit;
                }
            }
        }
        function pecahan2() {
            validasi();
            if (form.total.value=="") {
                a=eval(form.submit2.value); 
                b=0; 
                c=a+b 
                form.total.value = c; 
      
            }else{
                a=eval(form.total.value); 
                b=eval(form.submit2.value); 
                c=a+b 
                form.total.value = c; 
                if(c>form.price.value){
                    form.change_money.value = form.total.value - form.price.value; 
                    exit;
                }
            }
        }
        function pecahan3() {
            validasi();
            if (form.total.value=="") {
                a=eval(form.submit3.value); 
                b=0; 
                c=a+b 
                form.total.value = c; 
                if(c>form.price.value){
                    form.change_money.value = form.total.value - form.price.value; 
                    exit;
                }
      
            }else{
                a=eval(form.total.value); 
                b=eval(form.submit3.value); 
                c=a+b 
                form.total.value = c; 
                if(c>form.price.value){
                    form.change_money.value = form.total.value - form.price.value; 
                    exit;
                }
            }
        }
        function pecahan4() {
            validasi();
            if (form.total.value=="") {
                a=eval(form.submit4.value); 
                b=0; 
                c=a+b 
                form.total.value = c; 
                if(c>form.price.value){
                    form.change_money.value = form.total.value - form.price.value; 
                    exit;
                }
      
            }else{
                a=eval(form.total.value); 
                b=eval(form.submit4.value); 
                c=a+b 
                form.total.value = c; 
                if(c>form.price.value){
                    form.change_money.value = form.total.value - form.price.value; 
                    exit;
                }
            }
        }
       
    </script>
</head>
<body>
    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left"><h2>Vending Machine</h2></div>
                <!-- <div class="pull-right mb-2">
                <a class="btn btn-success" href="{{ route('product.create') }}"> Create Company</a>
                </div> -->
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <table class="table1">
            <tr>
                <th>No</th>
                <th>Item</th>
                <th>Price</th>
                <th>Stok</th>
                <!-- <th width="280px">Action</th> -->
            </tr>
            @foreach ($product as $items)
                <tr>
                    <td>{{ $items->id }}</td>
                    <td>{{ $items->name }}</td>
                    <td>{{ $items->price }}</td>
                    <td>{{ $items->stock }}</td>
                    <!-- <td>
                    <form action="{{ route('product.destroy',$items->id) }}" method="Post">
                    <a class="btn btn-primary" href="{{ route('product.edit',$items->id) }}">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    </td>  -->
                </tr>
            @endforeach
        </table>
        {!! $product->links() !!}
        <form action="{{ route('product.update','param') }}" method="POST" enctype="multipart/form-data" name="form">
            @csrf
            @method('PUT')
           
            <table class="table1">
              <tr>
                <th>
                <select name="product" id="product" class="form-control">
                @foreach ($product as $items)
                  <option style="display:none";>No Item </option>
                  <option value="{{ $items->name }}" 
                    data-id='{{ $items->id }}' 
                    data-items='{{ $items->name }}' 
                    data-price='{{ $items->price }}'
                    data-stock='{{ $items->stock }}'>{{ $items->id }}</option>

                @endforeach
                </select>
                </th>
                <th><input type="text" id="id_item"   name='id_item'   class='form-control' readonly placeholder='Id Item' style="display: none;"></th>
                <th><input type="text" id="item_name" name='item_name' class='form-control' readonly placeholder='Items'></th>
                <th><input type='text' id="price"     name='price'     class='form-control' placeholder='Price' readonly style="display: none;"></th>
                <th><input type='text' id="stock"     name='stock'     class='form-control' placeholder='Stock' readonly style="display: none;"></th>
              </tr>
            </table>
        
        
            <table class="table1">
                <tr>
                    <th>Input Money </th>
                    <th><input type='button' class='btn btn-danger' name='submit1' onclick='pecahan1()' value='2000'></th>
                    <th><input type='button' class='btn btn-danger' name='submit2' onclick='pecahan2()' value='5000'></th>
                    <th><input type='button' class='btn btn-danger' name='submit3' onclick='pecahan3()' value='10000'></th>
                    <th><input type='button' class='btn btn-danger' name='submit4' onclick='pecahan4()' value='20000'></th>

                </tr>
            </table>

            <table class="table1">
                <tr>
                    <th>Uang Bayar</th>
                    <th><input type='text' name='total' class='form-control' placeholder='Total' readonly ></th>
                    <th><input type='text' name='change_money' class='form-control' placeholder='Change Money' readonly></th>
                    <th><input type='submit' class='btn btn-primary' name='submit5' onclick='validasi_bayar()' value='Take'></th>
                    
                </tr>
            </table>
        </form>
        
                
            
        
        
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script type="text/javascript"> 
            // Ambil dari atribut data
      $(document).ready(function() {
        $('#product').on('change', function() {
          const selected = $(this).find('option:selected');
          const id_items = selected.data('id'); 
          const items = selected.data('items'); 
          const price = selected.data('price'); 
          const stock = selected.data('stock'); 

          $("#id_item").val(id_items);
          $("#item_name").val(items);
          $("#price").val(price);
          $("#stock").val(stock);
        });
      });
      
     
        </script>
</body>
</html>