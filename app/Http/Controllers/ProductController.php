<?php

namespace App\Http\Controllers;
use App\Models\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product['product'] = Product::orderBy('id','asc')->paginate(5);
        return view('product.index', $product);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
       
       $request->validate([
            'items_name' => 'required',
            'price' => 'required'
        ]);
        $company = Product::find($id);
        $company->stock = $company->stock-1;
        $company->save();
        return redirect()->route('product.index')
        ->with('sukses','Company Has Been updated successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return view('product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $id_item = $request->id_item;
        if ($id_item==""){
             return redirect()->route('product.index');
            exit();
        }else{
            $product = Product::find($request->id_item);
            $stock = $product->stock;
            if ($stock<=0){
                return redirect()->route('product.index')->with('sukses','Company Has Been updated successfully');
            }else{
                $product->stock = $product->stock-1;
                $product->save();
                return redirect()->route('product.index')
                ->with('sukses','Company Has Been updated successfully');
            }
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
